package com.palo;

import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.common.collect.ImmutableMap;
import com.google.pubsub.v1.ProjectTopicName;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

class Injector {
    private static Pubsub pubsub;
    private static ProjectTopicName topic;
    private static String project;

    public static final int FIRST_PHASE_IN_SECONDS = 120;
    public static final int JUMPING_TIME_IN_SECONDS = 1200;
    public static final int THIRD_PHASE_IN_SECONDS = 300;

    public static final int DELAY_BETWEEN_MESSAGES_IN_SECONDS = 1;

    public static final DateTimeFormatter DATE_TIME_FORMATTER =
            DateTimeFormat.forPattern("yyyyMMdd-HH:mm:ss.SSS");

    public static final String TIMESTAMP_ATTRIBUTE = "TIMESTAMP";

    private static PubsubMessage generateMessage() throws UnsupportedEncodingException {

        Long timestamp = System.currentTimeMillis();
        String timestampString = DATE_TIME_FORMATTER.print(timestamp);

        String event = "event-with-timestamp-" + timestampString;

        PubsubMessage pubsubMessage = new PubsubMessage().encodeData(event.getBytes("UTF-8"));
        pubsubMessage.setAttributes(
                ImmutableMap.of(
                        TIMESTAMP_ATTRIBUTE,
                        timestamp.toString()));
        return pubsubMessage;
    }

    public static void publishData(int publishDelay) throws Exception {
        List<PubsubMessage> pubsubMessages = new ArrayList<>();

        PubsubMessage message = generateMessage();
        String text = new String(message.decodeData());
        pubsubMessages.add(message);

        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setMessages(pubsubMessages);

        System.out.println(text + ": sending a messages after " + publishDelay + " delay");

        Thread.sleep(publishDelay);
        String sendingTs = DATE_TIME_FORMATTER.print(System.currentTimeMillis());

        pubsub.projects().topics().publish(topic.toString(), publishRequest).execute();
        System.out.println(text + ": sent at " + sendingTs);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 1) {
            System.out.println("Usage: Injector project-name topic-name");
            System.exit(1);
        }
        project = args[0];
        String topicName = args[1];

        // Create the PubSub client.
        pubsub = InjectorUtils.getClient();

        // Create the PubSub topic as necessary.
        topic = ProjectTopicName.of(project, topicName);
        InjectorUtils.createTopic(topic);

        System.out.println("Injecting to topic: " + topic);
        System.out.println("Starting Injector");

        for (int index = 0; index < FIRST_PHASE_IN_SECONDS; index++) {
            try {
                publishData(0);
            } catch (Exception e) {
                System.err.println(e);
            }
            Thread.sleep(DELAY_BETWEEN_MESSAGES_IN_SECONDS * 1000);
        }

        for (int index = 0; index < JUMPING_TIME_IN_SECONDS; index++) {
            final int delay = DELAY_BETWEEN_MESSAGES_IN_SECONDS * (JUMPING_TIME_IN_SECONDS - index);
            new Thread(
                    () -> {
                        try {
                            publishData(delay);
                        } catch (Exception e) {
                            System.err.println(e);
                        }
                    }).start();
            Thread.sleep(DELAY_BETWEEN_MESSAGES_IN_SECONDS);
        }

        for (int index = 0; index < THIRD_PHASE_IN_SECONDS; index++) {
            try {
                publishData(0);
            } catch (Exception e) {
                System.err.println(e);
            }
            Thread.sleep(DELAY_BETWEEN_MESSAGES_IN_SECONDS);
        }
    }
}
