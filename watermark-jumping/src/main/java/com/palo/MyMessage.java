package com.palo;

import org.apache.avro.reflect.Nullable;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;

import java.util.Objects;

@DefaultCoder(AvroCoder.class)
public class MyMessage {
    @Nullable
    String message;
    @Nullable
    Boolean isKeepalive;
    @Nullable
    Long timestamp;
    @Nullable
    Integer counter;

    public MyMessage() {
    }

    public MyMessage(String message, Boolean isKeepalive, Long timestamp, Integer counter) {
        this.message = message;
        this.isKeepalive = isKeepalive;
        this.timestamp = timestamp;
        this.counter = counter;
    }

    public Boolean isKeepalive() {
        return this.isKeepalive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }

        MyMessage message = (MyMessage) o;

        if (!this.message.equals(message.message)) {
            return false;
        }

        if (!this.isKeepalive.equals(message.isKeepalive)) {
            return false;
        }
        if (!this.counter.equals(message.counter)) {
            return false;
        }

        return this.timestamp.equals(message.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, isKeepalive, timestamp, counter);
    }

    @Override
    public String toString() {
        String result = timestamp + "," + counter + "," + message;
        return result;
    }

    public byte[] getBytes() {
        byte[] result = this.toString().getBytes();
        return result;
    }

    public static MyMessage CreateKeepalive(Long timestamp, int counter) {
        return new MyMessage("keepalive!", true, timestamp, counter);
    }
}
