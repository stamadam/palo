package com.palo;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.gax.rpc.NotFoundException;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.PubsubScopes;
import com.google.cloud.pubsub.v1.TopicAdminClient;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.Topic;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

class InjectorUtils {

    private static final String APP_NAME = "injector";

    /**
     * Builds a new Pubsub client and returns it.
     */
    public static Pubsub getClient(final HttpTransport httpTransport, final JsonFactory jsonFactory)
            throws IOException {
        checkNotNull(httpTransport);
        checkNotNull(jsonFactory);
        GoogleCredential credential =
                GoogleCredential.getApplicationDefault(httpTransport, jsonFactory);
        if (credential.createScopedRequired()) {
            credential = credential.createScoped(PubsubScopes.all());
        }
        HttpRequestInitializer initializer = new RetryHttpInitializerWrapper(credential);
        return new Pubsub.Builder(httpTransport, jsonFactory, initializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    /**
     * Builds a new Pubsub client with default HttpTransport and JsonFactory and returns it.
     */
    public static Pubsub getClient() throws IOException {
        return getClient(Utils.getDefaultTransport(), Utils.getDefaultJsonFactory());
    }

    /**
     * Create a topic if it doesn't exist.
     */
    public static void createTopic(ProjectTopicName fullTopicName) throws IOException {
        TopicAdminClient topicAdminClient = TopicAdminClient.create();
        try {
            topicAdminClient.getTopic(fullTopicName);
        } catch (NotFoundException e) {
            topicAdminClient.createTopic(fullTopicName);
        }
    }
}
