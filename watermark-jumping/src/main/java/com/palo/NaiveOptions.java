package com.palo;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.StreamingOptions;
import org.apache.beam.sdk.options.Validation;

public interface NaiveOptions extends StreamingOptions {
    @Description("Pub/Sub subscription to read from")
    @Validation.Required
    String getSubscription();

    void setSubscription(String value);

    @Description("Numeric value of fixed window duration, in minutes")
    @Default.Integer(1)
    Integer getWindowDuration();

    void setWindowDuration(Integer value);

    @Description("File prefix, for example: gs://palo-258218/naivefile-")
    @Default.String("gs://palo-258218/naivefile-")
    String getFilePrefix();

    void setFilePrefix(String value);

}