package com.palo;

import org.apache.beam.sdk.io.FileBasedSink;
import org.apache.beam.sdk.io.FileBasedSink.FilenamePolicy;
import org.apache.beam.sdk.io.FileBasedSink.OutputFileHints;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.fs.ResolveOptions.StandardResolveOptions;
import org.apache.beam.sdk.io.fs.ResourceId;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.transforms.windowing.PaneInfo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PDone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.annotation.Nullable;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * A {@link DoFn} that writes elements to files with names deterministically derived from the lower
 * and upper bounds of their key (an {@link IntervalWindow}).
 *
 * <p>This is test utility code, not for end-users, so examples can be focused on their primary
 * lessons.
 */
public class WriteToFile extends PTransform<PCollection<MyMessage>, PDone> {
    public static final DateTimeFormatter FORMATTER =
            DateTimeFormat.forPattern("yyyyMMdd-HH:mm");

    private String filenamePrefix;
    @Nullable
    private Integer numShards;

    public WriteToFile(String filenamePrefix) {
        this.filenamePrefix = filenamePrefix;
        this.numShards = 1;
    }

    class MessageTofile extends SimpleFunction<MyMessage, String> {
        @Override
        public String apply(MyMessage input) {
            return input.message;
        }
    }

    @Override
    public PDone expand(PCollection<MyMessage> input) {
        ResourceId resource = FileBasedSink.convertToFileResourceIfPossible(filenamePrefix);
        TextIO.Write write =
                TextIO.write()
                        .to(new PerWindowFiles(resource))
                        .withTempDirectory(resource.getCurrentDirectory())
                        .withWindowedWrites();
        if (numShards != null) {
            write = write.withNumShards(numShards);
        }

        return input.apply(MapElements.via(new MessageTofile())).apply(write);
    }

    /**
     * A {@link FilenamePolicy} produces a base file name for a write based on metadata about the data
     * being written. This always includes the shard number and the total number of shards. For
     * windowed writes, it also includes the window and pane index (a sequence number assigned to each
     * trigger firing).
     */
    public static class PerWindowFiles extends FilenamePolicy {

        private final ResourceId baseFilename;

        public PerWindowFiles(ResourceId baseFilename) {
            this.baseFilename = baseFilename;
        }

        public String filenamePrefixForWindow(IntervalWindow window) {
            return new StringBuilder()
                    .append(baseFilename.isDirectory() ? "" : firstNonNull(baseFilename.getFilename(), ""))
                    .append('-')
                    .append(FORMATTER.print(window.end()))
                    .toString();
        }

        @Override
        public ResourceId windowedFilename(
                int shardNumber,
                int numShards,
                BoundedWindow window,
                PaneInfo paneInfo,
                OutputFileHints outputFileHints) {
            IntervalWindow intervalWindow = (IntervalWindow) window;

            String filename = new StringBuilder().append(filenamePrefixForWindow((intervalWindow)))
                    .append('-')
                    .append(outputFileHints.getSuggestedFilenameSuffix())
                    .toString();

            return baseFilename
                    .getCurrentDirectory()
                    .resolve(filename, StandardResolveOptions.RESOLVE_FILE);
        }

        @Override
        public ResourceId unwindowedFilename(
                int shardNumber, int numShards, OutputFileHints outputFileHints) {
            throw new UnsupportedOperationException("Unsupported.");
        }
    }
}
