package com.palo;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.joda.time.Duration;

public class Naive {
    public static final String TIMESTAMP_ATTRIBUTE = "TIMESTAMP";

    public static void main(String[] args) throws Exception {

        NaiveOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(NaiveOptions.class);
        options.setStreaming(true);
        Pipeline pipeline = Pipeline.create(options);

        // Read Events from Pub/Sub using custom timestamps
        pipeline.apply(
                PubsubIO.readStrings().withTimestampAttribute(TIMESTAMP_ATTRIBUTE)
                        .fromSubscription(options.getSubscription()))
                .apply("Parse messages", ParDo.of(new NaiveParseEvent()))
                .apply("Split to minutes", Window.into(
                        FixedWindows.of(Duration.standardMinutes(options.getWindowDuration()))))
                .apply(new WriteToFile(options.getFilePrefix()));

        pipeline.run();
    }
}
