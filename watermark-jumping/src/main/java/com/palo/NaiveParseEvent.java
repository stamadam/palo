package com.palo;

import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class NaiveParseEvent extends DoFn<String, MyMessage> {

    // Log and count parse errors.
    private static final Logger LOG = LoggerFactory.getLogger(NaiveParseEvent.class);
    private final Counter numParseErrors = Metrics.counter("main", "ParseErrors");

    @ProcessElement
    public void processElement(ProcessContext c) {
        String message = c.element();
        try {
            Instant d = c.timestamp();
            c.output(new MyMessage(message, false, d.getMillis(), 1));
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            numParseErrors.inc();
            LOG.info("Parse error on {}, {}", c.element(), e.getMessage());
        }
    }
}
